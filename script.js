const menuItems = [
  "Bruschette Classiche",
  "Pizza Margherita Trancio",
  "Guacamole con Nachos",
  "Babaganoush con Piadina Croccante",
  "Tortino di Ceci e Verdure con Salsa alle Erbe"
  // Aggiungi altri elementi del menu qui...
];

const menuPrices = {}; // Oggetto per memorizzare i prezzi dei piatti

const cart = [];

function populateMenu() {
  const antipastiList = document.getElementById("antipasti");

  menuItems.forEach(item => {
    const price = generateRandomPrice(); // Genera un prezzo casuale
    menuPrices[item] = price; // Memorizza il prezzo associato al piatto
    const li = document.createElement("li");
    li.textContent = `${item} - €${price}`;
    li.addEventListener("click", () => addToCart(item));
    antipastiList.appendChild(li);
  });
}

function generateRandomPrice() {
  return (Math.floor(Math.random() * 5) + 6); // Genera un numero casuale tra 6 e 10
}

function addToCart(item) {
  cart.push(item);
  displayCart();
}

function displayCart() {
  const cartList = document.getElementById("cart-items");
  cartList.innerHTML = "";
  let totalPrice = 0;

  cart.forEach(item => {
    const li = document.createElement("li");
    li.textContent = `${item} - €${menuPrices[item]}`;
    cartList.appendChild(li);
    totalPrice += menuPrices[item];
  });

  document.getElementById("total").textContent = `Totale: €${totalPrice}`;
}

function checkout() {
  alert("Grazie per l'ordine! Totale: €" + calculateTotalPrice());
  cart.length = 0; // Svuota il carrello dopo il checkout
  displayCart(); // Aggiorna la visualizzazione del carrello
}

function calculateTotalPrice() {
  let totalPrice = 0;

  cart.forEach(item => {
    totalPrice += menuPrices[item];
  });

  return totalPrice;
}

// Inizializza il menu all'avvio della pagina
populateMenu();
